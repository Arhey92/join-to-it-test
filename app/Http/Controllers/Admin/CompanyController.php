<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Requests\StoreCompany;
use App\Http\Requests\UpdateCompany;
use App\Logo;
use App\Mail\NewCompanyEntered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('admin.companies.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCompany $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreCompany $request)
    {
        $storedLogo = false;
        if($request->hasFile('logo')){
            $logo = new Logo();
            $storedLogo = $logo->storeLogo($request->file('logo'));
        }

        $company = Company::create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo_id' => ($storedLogo)?$storedLogo->id:null
        ]);

        Mail::to($request->user())->send(new NewCompanyEntered($company));

        return redirect(route('companies.index'))->with('success', 'Company was successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        return view('admin.companies.show', ['company' => $company]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('admin.companies.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCompany $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateCompany $request, $id)
    {
        $company = Company::find($id);

        $storedLogo = $company->logo;
        $oldLogoId = false;
        if($request->hasFile('logo')){
            //delete old logo from storage and from table
            if(!is_null($company->logo)){
                $oldLogoId = $company->logo->id;
            }

            //store new logo
            $logo = new Logo();
            $storedLogo = $logo->storeLogo($request->file('logo'));
        }

        $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo_id' => ($storedLogo)?$storedLogo->id:null
        ]);

        if($oldLogoId){
            Logo::deleteLogo($oldLogoId);
        }

        return redirect(route('companies.index'))->with('success', 'Company was successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);

        if(!is_null($company)){
            if(!is_null($company->logo_id)){
                Logo::deleteLogo($company->logo_id);
            }

            $company->delete();
        }

        return redirect(route('companies.index'));
    }
}
