@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="{{route('companies.create')}}" class="btn btn-success">Create new company</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{route('companies.index')}}">Companies</a>
        </li>
    </ul>

    <h1>Companies</h1>

    @if(!is_null($companies))
        <table id="companies">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Logo</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Website</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>
                        <a href="{{route('companies.show', $company->id)}}">
                            @if(!is_null($company->logo))
                                <img class="img-fluid" style="max-width: 25%; max-height: 25%" src="{{$company->logo->getUrl()}}">
                            @else
                                <img class="img-fluid" style="max-width: 25%; max-height: 25%" src="{{asset('images/no-image-icon.png')}}">
                            @endif
                        </a>
                    </td>
                    <td>{{$company->name}}</td>
                    <td>{{$company->email}}</td>
                    <td>{{$company->website}}</td>
                    <td class="row">
                        <div class="btn-group" role="group">
                            <a href="{{route('companies.show', $company->id)}}" class="btn btn-info">Show</a>
                            <a class="btn btn-warning" href="{{route('companies.edit', $company->id)}}">Edit</a>
                            <form method="POST" action="{{route('companies.destroy', $company->id)}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $companies->links() }}
    @else
        Empty
    @endif
@endsection

@section('custom-script')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#companies').DataTable();
        } );
    </script>
@endsection