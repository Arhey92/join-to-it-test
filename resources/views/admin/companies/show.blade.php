@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>
        </div>
    </nav>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{route('companies.index')}}">Companies</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled"> > </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{$company->name}}</a>
        </li>
    </ul>

    <h1>{{$company->name}}</h1>
    <a href="{{route('companies.index')}}" class="btn btn-secondary"><- Back</a>

    <div class="row">
        <div class="col-3">
            @if(!is_null($company->logo))
                <img class="img-fluid" src="{{$company->logo->getUrl()}}">
            @else
                <img class="img-fluid" src="{{asset('images/no-image-icon.png')}}">
            @endif
        </div>
        <div class="col-9">
            <p><b>Name:</b> {{$company->name}}</p>
            <p><b>Email:</b> {{$company->email}}</p>
            <p><b>Website:</b> {{$company->website}}</p>
            <div class="row">
                <div class="btn-group" role="group">
                    <a class="btn btn-warning" href="{{route('companies.edit', $company->id)}}">Edit</a>
                    <form method="POST" action="{{route('companies.destroy', $company->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection