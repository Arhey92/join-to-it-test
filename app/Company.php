<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'email',
        'logo_id',
        'website'
    ];

    public function logo()
    {
        return $this->hasOne('App\Logo', 'id', 'logo_id');
    }
}
