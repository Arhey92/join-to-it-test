@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>
        </div>
    </nav>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{route('employees.index')}}">Employees</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled"> > </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{$employee->first_name}} {{$employee->last_name}}</a>
        </li>
    </ul>

    <h1>{{$employee->first_name}} {{$employee->last_name}}</h1>
    <a href="{{route('employees.index')}}" class="btn btn-secondary"><- Back</a>

    <div class="row">
        <div class="col-9">
            <p><b>Name:</b> {{$employee->name}}</p>
            <p><b>Email:</b> {{$employee->email}}</p>
            <p><b>Website:</b> {{$employee->website}}</p>
            <p><b>Company:</b> {{$employee->company->name}}</p>
            <div class="row">
                <div class="btn-group" role="group">
                    <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-warning">Edit</a>
                    <form method="POST" action="{{route('employees.destroy', $employee->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection