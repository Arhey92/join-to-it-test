@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <h1>Employees</h1>

    <a href="{{route('employees.index')}}" class="btn btn-secondary"><- Back</a>

    <div class="row">
        <form class="col-6" method="POST" action="{{ route('employees.update', $employee->id) }}">
            @csrf
            {{method_field('PUT')}}

            <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="{{$employee->first_name}}" required>
            </div>

            <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="{{$employee->last_name}}" required>
                <p class="help-block">* Not required</p>
            </div>

            <div class="form-group">
                <label for="company_id">Company</label>
                <select class="form-control" id="company_id" name="company_id">
                    @foreach($companies as $company)
                        @if($employee->company_id == $company->id)
                            <option value="{{$company->id}}" selected>{{$company->name}}</option>
                        @else
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="{{$employee->email}}">
                <p class="help-block">* Not required</p>
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="{{$employee->phone}}">
                <p class="help-block">* Not required</p>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    Update
                </button>
            </div>
        </form>
    </div>

@endsection