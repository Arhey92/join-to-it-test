<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Logo extends Model
{
    protected $fillable = [
        'name',
        'path',
        'extension'
    ];

    public function storeLogo(UploadedFile $logo, $folder = 'companies')
    {
        $name = time().'-'.$logo->getClientOriginalName();
        $path = Storage::disk('public')->putFileAs($folder, $logo, $name);

        $storedLogo = Logo::create([
            'name' => $name,
            'path' => $path,
            'extension' => $logo->getClientOriginalExtension()
        ]);

        return $storedLogo;
    }

    public static function deleteLogo($id)
    {
        $logo = Logo::find($id);
        Storage::disk('public')->delete($logo->path);
        $logo->delete();

        return true;
    }

    public function getUrl()
    {
        return Storage::url($this->path);
    }
}
