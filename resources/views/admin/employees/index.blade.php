@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="{{route('employees.create')}}" class="btn btn-success">Create new employee</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{route('employees.index')}}">Employees</a>
        </li>
    </ul>

    <h1>Employees</h1>

    @if(!is_null($employees))
        <table id="employees">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First name</th>
                <th scope="col">Last name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach($employees as $employee)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$employee->first_name}}</td>
                    <td>{{$employee->last_name}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td class="row">
                        <div class="btn-group" role="group">
                            <a href="{{route('employees.show', $employee->id)}}" class="btn btn-info">Show</a>
                            <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-warning">Edit</a>
                            <form method="POST" action="{{route('employees.destroy', $employee->id)}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $employees->links() }}
    @else
        Empty
    @endif
@endsection

@section('custom-script')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#employees').DataTable();
        } );
    </script>
@endsection