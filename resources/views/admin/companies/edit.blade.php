@extends('layouts.admin.admin')

@include('layouts.admin.errors.errors')

@section('content')
    <h1>Edit company: {{$company->name}}</h1>

    <a href="{{route('companies.index')}}" class="btn btn-secondary"><- Back</a>

    <form method="POST" action="{{ route('companies.update', $company->id) }}" enctype="multipart/form-data">
        @csrf
        {{method_field('PUT')}}

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$company->name}}" required>
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{$company->email}}">
            <p class="help-block">* Not required</p>
        </div>

        <div class="form-group">
            <label for="website">Website</label>
            <input type="text" class="form-control" id="website" name="website" value="{{$company->website}}">
            <p class="help-block">* Not required</p>
        </div>

        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="logo" name="logo">
                <label class="custom-file-label" for="logo">Upload logo</label>
                <p class="help-block">* Not required</p>
            </div>
            <div class="row">
                <div class="col-6">
                    Current logo:
                    <img id="current_logo" src="{{$company->logo->getUrl()}}" alt="current logo" class="col-3"/>
                </div>
                <div class="col-6">
                    New logo:
                    <img id="new_logo" src="#" alt="new logo" class="col-3"/>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Update
            </button>
        </div>
    </form>
@endsection

@section('custom-script')
    <script>
        $( document ).ready(function() {
            $('#logo').change(function(){
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
                {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#new_logo').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
                else
                {
                    $('#new_logo').attr('src', '#');
                }
            });
        });
    </script>
@endsection